package utils;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import io.restassured.response.Response;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.when;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static net.javacrumbs.jsonunit.core.util.ResourceUtils.resource;
import static org.junit.Assert.assertEquals;
import static utils.AssertionUtils.jsonResponse;

/**
 * created by michaelv on 20/02/2019
 **/
public class ApiUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ApiUtils.class);

    // If you want the test to not compare response and expected, instead just write response to filename + ^response.
    private static boolean compare = true;

    // If you want the test to write the expected responses back to update the original source files set this to true.
    private static boolean overwrite = false;

    // Debugging purposes - if we ever wanted to debug out all the failures you can log out a list of endpoints thare failing in the tests.
    private static Map<String, Response> failures = new HashMap<>();

    //API BaseURL
    private static String baseURL = PropertyUtils.getProperty("baseURI");

    private static Response initialResponse;

    private static void callAPI(String endpoint) {
        initialResponse = when().get(baseURL + endpoint);
        if (initialResponse.contentType().equals("text/html; charset=utf-8")) {
            AssertionUtils.parseString(initialResponse.asString());
        }
    }

    public static void apiAssertion(String endpoint, String expected, int statusCode) throws IOException {
        LOG.info(("Making a call to the API......"));

        if (compare && !overwrite) {
            AssertionUtils.getResultFile(expected).delete();
        }
        callAPI(endpoint);
        try {
            initialResponse.then()
                .statusCode(statusCode);
            // Can't validate Content type as currently incorrect
            //                .contentType(ContentType.JSON);
            if (compare) {
                assertJsonEquals(jsonResponse,
                    resource("expected/" + expected));
            } else {
                Assert.fail("Not comparing response, write direct to file!");
            }
        } catch (AssertionError error) {
            FileUtils.writeStringToFile(AssertionUtils.getResultFile(expected), initialResponse.prettyPrint());
            failures.put(baseURL + endpoint, initialResponse);
            throw error;
        }
    }

    public static void validateItemSize(String endpoint, String key, int size) {
        callAPI(endpoint);
        JSONArray array = jsonResponse.getJSONArray(key);
        assertEquals(array.length(), size);
    }

    public static void assertFieldsinResponse(String endpoint, String key, String field) {
        callAPI(endpoint);
        String json = jsonResponse.toString();
        ReadContext ctx = JsonPath.parse(json);
        String item = ctx.read(key);
        assertEquals(item, field);
    }
}
