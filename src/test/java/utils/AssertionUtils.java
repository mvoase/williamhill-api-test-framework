package utils;

import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.File;

/**
 * created by michaelv on 20/02/2019
 **/
public class AssertionUtils {

    private static String testExpectedDirectory = PropertyUtils.getProperty("testExpectedDirectory");

    // If you want the test to write the expected responses back to update the original source files set this to true.
    private static boolean overwrite = false;

    // Parsed response
    public static JSONObject jsonResponse;

    public static File getResultFile(String expectedURI) {

        String path = testExpectedDirectory + File.separator + expectedURI;

        if (!overwrite) {
            path = path.replaceAll("\\.json", "^response.json");
        }
        return new File(path);
    }

    public static void parseString(String content) {
        String s = Jsoup.parse(content).text();
        createJSON(s);
    }

    private static JSONObject createJSON(String json) {
        return jsonResponse = new JSONObject(json);
    }
}
