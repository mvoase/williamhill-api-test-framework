package utils;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import static java.util.Optional.ofNullable;

/**
 * created by michaelv on 20/02/2019
 **/
public class PropertyUtils {

    private static final ResourceBundle rb = ResourceBundle.getBundle("application");

    public static String getProperty(String key) {
        return System.getProperty(key, loadProperty(key, null));
    }

    private static String loadProperty(String key, String defaultValue) {
        try {
            return rb.getString(key);
        } catch (MissingResourceException missing) {
            return ofNullable(defaultValue)
                .orElseThrow(() -> missing);
        }
    }
}
