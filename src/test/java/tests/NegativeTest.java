package tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import utils.ApiUtils;

import java.io.IOException;

/**
 * created by michaelv on 21/02/2019
 **/
@RunWith(JUnit4.class)
public class NegativeTest {

    private final String negativeExpectedDirectory = "negative";

    @Test
    public void negativeTests() throws IOException {
        // This fails as the status code returned is 200 not 404 and the body is returned empty.
        ApiUtils.apiAssertion("event/80", negativeExpectedDirectory + "/" + "invalid_event.json", 404);
    }


}
