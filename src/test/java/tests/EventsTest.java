package tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import utils.ApiUtils;

import java.io.IOException;

/**
 * created by michaelv on 21/02/2019
 **/
@RunWith(JUnit4.class)
public class EventsTest {

    private final String eventExpectedDirectory = "events";

    @Test
    public void payloadTest() throws IOException {
        ApiUtils.apiAssertion("events", eventExpectedDirectory + "/" + "event_collection.json", 200);
        ApiUtils.apiAssertion("event/1", eventExpectedDirectory + "/" + "event_detail.json", 200);
    }

    @Test
    public void eventDataValidationTest() {
        ApiUtils.validateItemSize("events", "events", 4);
        ApiUtils.assertFieldsinResponse("events", "$.events[0].category", "Football");
        ApiUtils.assertFieldsinResponse("events", "$.events[3].category", "Tennis");
    }
}
